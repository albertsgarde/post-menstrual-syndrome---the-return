CC=gcc
LDFLAGS=-lopenblas
CFLAGS= -Wall
# Requires: "module load openblas"
#CPPFLAGS=-I$(MODULE_OPENBLAS_BASE_DIR)/include
#LDFLAGS=-lopenblas -L$(MODULE_OPENBLAS_BASE_DIR)/lib
#LDLIBS=-l$(MODULE_OPENBLAS_LIB)

all: lssolve tests

lssolve: lssolve.o call_dgels.o matrix_io.o
	$(CC) $(CFLAGS) lssolve.o call_dgels.o matrix_io.o -o lssolve $(LDFLAGS)

tests: tests.o matrix_io.o call_dgels.o
	$(CC) $(CFLAGS) tests.o call_dgels.o matrix_io.o -o tests $(LDFLAGS)

lssolve.o: lssolve.c
	$(CC) $(CFLAGS) -c lssolve.c -o lssolve.o $(LDFLAGS)

tests.o: tests.c
	$(CC) $(CFLAGS) -c tests.c -o tests.o $(LDFLAGS)

matrix_io.o: matrix_io.c
	$(CC) $(CFLAGS) -c matrix_io.c -o matrix_io.o $(LDFLAGS)

call_dgels.o: call_dgels.c
	$(CC) $(CFLAGS) -c call_dgels.c -o call_dgels.o $(LDFLAGS)

clean:
	rm -f *.o lssolve tests
