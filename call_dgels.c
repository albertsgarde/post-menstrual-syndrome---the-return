#include "matrix_io.h"

/* C prototype for LAPACK routine DGELS */
void dgels_(
	const char * trans,  /* 'N' or 'T'             */
	const int * m,       /* rows in A              */
	const int * n,       /* cols in A              */
	const int * nrhs,    /* cols in B              */
	double * A,          /* array A                */
	const int * lda,     /* leading dimension of A */
	double * B,          /* array B                */
	const int * ldb,     /* leading dimension of B */
	double * work,       /* workspace array        */
	int * lwork,         /* workspace size         */
	int * info           /* status code            */
);

/* call_dgels : wrapper for LAPACK's DGELS routine

Purpose:
Solves the least-squares problem

   minimize  || A*x-b ||_2^2

using LAPACK's DGELS routine. Upon exit, the input vector b is overwriten, i.e.,
the leading n elements of b contain the solution x.

Return value:
The function returns the output `info` from DGELS with the
following exceptions: the return value is

	-12 if the input A is NULL and/or the input B is NULL
	-13 if A is not a tall matrix
	-14 if the dimensions of A and b are incompatible
	-15 in case of memory allocation errors.
*/


int call_dgels(matrix_t * A, vector_t * b) {
    // Assert all pointers are not-NULL
    if(!A || !A->A[0] || !b || !b->v)
        return -12;
    
    // Assert that the matrix is tall or rectangular - otherwise matrix would not have full rank
    if(A->m < A->n)
        return -13;

    // Assert valid matrix-vector dimmensions
    if (A->m != b->n)
        return -14;

    int dgels_workspace_size = (A->n == 0 || A->m == 0) ? 1 : 2*(A->m);
   
    // Allocate and validate memory allocation for workspace
    double* dgels_workspace = (double *)malloc(sizeof(double)*dgels_workspace_size);
    if (!dgels_workspace)
        return -15;

    // The matrix is traposed for correct indexing, as A is a row-major matrix and dgels expects a column-major matrix.
    char transpose = 'T';

    // As A is trasposed, m and n are swapped
    int m = A->n; 
    int n = A->m;
 
    int num_right_hand_sides = 1;

    // Stride for transposed matrix A, and stride of vector b (as b has a row-major memory layout)
    int leading_dimension_A = m;
    int leading_dimension_b = n;

    int info;

    dgels_(&transpose, &m, &n, &num_right_hand_sides, A->A[0], &leading_dimension_A, b->v, &leading_dimension_b, dgels_workspace, &dgels_workspace_size, &info);

    free(dgels_workspace);

    return info;
}
