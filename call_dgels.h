#ifndef CALL_DGELS_H
#define CALL_DGELS_H

#include "matrix_io.h"

int call_dgels(matrix_t * A, vector_t * b);

#endif
