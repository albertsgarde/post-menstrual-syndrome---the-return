#include <stdlib.h>
#include <stdio.h>
#include "matrix_io.h"
#include "call_dgels.h"

int main(int argc, char *argv[]) {
    if (argc != 4)
    {
        fprintf(stderr,"Usage: %s A b x\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Input arguments
    char* A_path = argv[1];
    char* b_path = argv[2];
    char* x_path = argv[3];

    // Allocate and valdiate memory allocation for vector in b_path
    vector_t* b = read_vector(b_path);
    if (b == NULL)
    {
        fprintf(stderr,"Could not read vector from file.\n");
        return EXIT_FAILURE;
    }

    // Allocate and valdiate memory allocation for matrix in A_path
    matrix_t* A = read_matrix(A_path);
    if (A == NULL)
    {
        free_vector(b);
        fprintf(stderr,"Could not read matrix from file.\n");
        return EXIT_FAILURE;
    }

    // Solve least squares problem
    int info = call_dgels(A, b);

    // Log potential error in call_dgels to STDERR
    if (info != 0)
    {
        if (info > 0)
            fprintf(stderr, "The matrix A does not have full rank. Its %d'th diagonal is 0.\n", info);
        else if (info == -13)
            fprintf(stderr, "A is not a tall matrix.\n");
        else if (info == -14)
            fprintf(stderr, "The matrix A and the vector b have incompatible dimensions.\n");
        else if (info == -15)
            fprintf(stderr, "Failed to allocate memory for dgels_' workspace.\n");
        else if(info < 0)
            fprintf(stderr, "Illegal value in %d'th argument\n", -info);

        free_vector(b);
        free_matrix(A);
        
        return EXIT_FAILURE;
    }
    
    // Change length of vector
    b->n = A->n;

    // Write result from call_dgels (in the vector b) to x_path.
    // Potential errors are written to STDERR.
    int vector_write_error = write_vector(x_path, b);
    if (vector_write_error == MATRIX_IO_ILLEGAL_INPUT)
    {
        free_vector(b);
        free_matrix(A);

        fprintf(stderr, "Error in function. b should not be null.\n");
        return EXIT_FAILURE;
    }

    free_vector(b);
    free_matrix(A);

    return EXIT_SUCCESS;
}
