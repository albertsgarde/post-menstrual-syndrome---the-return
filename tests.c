#include <stdio.h>
#include "matrix_io.h"
#include "call_dgels.h"

int main(void)
{
    // Test NULL input parameters
    {
        // matrix vector dimmensions
        unsigned long m = 10;
        unsigned long n = 10;

        // Allocate memory for vector and matrix
        vector_t *b = malloc_vector(n);
        if (!b)
        {
            fprintf(stderr, "Memory allocation failure\n");
            return EXIT_FAILURE;
        }
        
        matrix_t *A = malloc_matrix(m, n);
        if (!A)
        {
            fprintf(stderr, "Memory allocation failure\n");
            free_vector(b);
            return EXIT_FAILURE;
        }

        /* Negative test for input validation */
        if ( call_dgels(NULL, b) != -12 || call_dgels(A, NULL) != -12 || call_dgels(NULL, NULL) != -12 )
        {
            fprintf(stderr, "Should return -12 on NULL input parameters\n");
            return EXIT_FAILURE;
        }
    }
    
    /*
    // Test tall matrix
    {
        // matrix vector dimmensions
        unsigned long m = 5;
        unsigned long n = 10;

        // Allocate memory for vector and matrix
        vector_t *b = malloc_vector(n);
        if (!b)
        {
            fprintf(stderr, "Memory allocation failure\n");
            return EXIT_FAILURE;
        }

        matrix_t *A = malloc_matrix(m, n);
        if (!A)
        {
            fprintf(stderr, "Memory allocation failure\n");
            free_vector(b);
            return EXIT_FAILURE;
        }

        if ( call_dgels(A, b) != -13)
        {
            fprintf(stderr, "Should return -13 if m <= n\n");
            return EXIT_FAILURE;
        }
    }

    // Test incompatible matrix vector dimmenions
    {
        // matrix vector dimmensions
        unsigned long m = 10;
        unsigned long n = 5;
        unsigned long p = 2;

        // Allocate memory for vector and matrix
        vector_t *b = malloc_vector(n);
        if (!b)
        {
            fprintf(stderr, "Memory allocation failure\n");
            return EXIT_FAILURE;
        }

        matrix_t *A = malloc_matrix(m, p);
        if (!A)
        {
            fprintf(stderr, "Memory allocation failure\n");
            free_vector(b);
            return EXIT_FAILURE;
        }

        if ( call_dgels(A, b) != -14)
        {
            fprintf(stderr, "Should return -14 on incompatible matrix vector dimmenions\n");
            return EXIT_FAILURE;
        }
    }

    */
    // Test correctness
    {
        // Load test data
        vector_t *b = read_vector("testdata/b.txt");
        if (!b)
        {
            fprintf(stderr, "Could not load testdata\n");
            return EXIT_FAILURE;
        }
        print_vector(b);

        vector_t *x = read_vector("testdata/x.txt");
        if (!x)
        {
            fprintf(stderr, "Could not load testdata\n");
            return EXIT_FAILURE;
        }
        print_vector(x);

        matrix_t *A = read_matrix("testdata/A.txt");
        if (!A)
        {
            fprintf(stderr, "Could not load testdata\n");
            return EXIT_FAILURE;
        }
        print_matrix(A);

        call_dgels(A, b);

        for(size_t i = 0; i < b->n; i++)
        {
            if (b->v[i] != x->v[i]) 
            {
                printf("%lf, %lf\n", b->v[i], x->v[i]);
                //fprintf(stderr, "Elements started differing at %ld\n", i);
                //return EXIT_FAILURE;
            }
        }
    }

    return EXIT_SUCCESS;
}
